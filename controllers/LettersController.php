<?php

namespace app\controllers;

use app\models\Letter;
use Yii;
use yii\web\Controller;
use app\models\LetterForm;
use yii\imagine\Image;
use gietos\yii\Dadata\Client;

class LettersController extends Controller
{
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LetterForm();

        if ($model->load(Yii::$app->request->post()) && $model->addLetter()) {

            return $this->render('created', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

//    public function actionEdit()
//    {
//        if (Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LetterForm();
//
//        if ($model->load(Yii::$app->request->post()) && $model->addLetter()) {
//
//            return $this->render('created', [
//                'model' => $model,
//            ]);
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }

    public function actionCabinet()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('cabinet', [
            'model' => $model,
        ]);
    }

    public function actionDownload($file) {
        if (file_exists($file)) {
            return \Yii::$app->response->sendFile($file);
        }
        throw new \Exception('File not found');
    }

//    public function actionDownloadById($id) {
//        $file = Letter::findOne(['id' => $id])->pdf_link;
//        if (file_exists($file)) {
//            return \Yii::$app->response->sendFile($file);
//        }
//        throw new \Exception('File not found');
//    }

    public function actionView($file)
    {
        if (file_exists($file)) {
            return Yii::$app->getResponse()->redirect($file);
//            return \Yii::$app->response->sendFile($file, ['inline'=>true]);
        }
        throw new \Exception('File not found');
    }

    public function actionDelete($id){
        Letter::deleteAll(['id' => $id]);

        return $this->render('cabinet', [
            'model' => $model,
        ]);
    }

    public function actionGetThumbnail()
    {
        $template_id = Yii::$app->request->post('template_id');
        $size = Yii::$app->request->post('size');

        $default_size = 500;
        $root_path = '@webroot/';
        $image_name = 'letter'.$template_id;
        $image_dir = Yii::getAlias($root_path.'letters/opacity/');
        $image_extension = '.png';
        $image = $image_dir.$image_name.$image_extension;


        $size_x = $size ? $size : $default_size;
        $size_y = $size_x*1.414;
        $thumb_path = Yii::getAlias('letters/opacity/thumbnails/'.$image_name.'-'.$size_x.'x'.round($size_y).$image_extension);
        $full_thumb_path = Yii::getAlias($root_path.$thumb_path);

        if (!file_exists($full_thumb_path)) {
            Image::thumbnail($image, $size_x, $size_y)
                ->save($full_thumb_path, ['quality' => 100]);
        }

        return $thumb_path;
    }
}
