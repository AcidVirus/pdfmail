<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegistrationForm;
use app\models\LetterForm;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin($reg_msg = null)
    {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::toRoute('letters/cabinet'));
        }

        $login_model = new LoginForm();
        if ($login_model->load(Yii::$app->request->post()) && $login_model->login()) {
            return Yii::$app->getResponse()->redirect(Url::toRoute('letters/cabinet'));
        }

        $reg_model = new RegistrationForm();
        if ($reg_model->load(Yii::$app->request->post()) && $reg_model->register()) {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['site/login', 'reg_msg' => 'Вы успешно зарегистрировались!']));
        }

        $login_model->password = '';
        return $this->render('login', [
            'login_model' => $login_model,
            'reg_model' => $reg_model,
            'reg_msg' => $reg_msg
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return Yii::$app->getResponse()->redirect(Url::toRoute('site/login'));
    }

//    public function actionRegistration()
//    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new RegistrationForm();
//        if ($model->load(Yii::$app->request->post()) && $model->register()) {
//            return Yii::$app->getResponse()->redirect(Url::toRoute('site/login'));
//        }
//
//        $model->password = '';
//        return $this->render('registration', [
//            'model' => $model,
//        ]);
//    }
}
