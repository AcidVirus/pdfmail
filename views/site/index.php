<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="site-index h-100" id="start">
    <div class="row h-100 align-items-center">
        <div class="col">
            <h1 class="col-12 text-center">PDF Mail</h1>
            <h2 class="col-12 text-center">Создание конвертов для писем ещё никогда не было столь легким</h2>
                <?=Html::a('Начать', Url::toRoute('site/login'), ['class' => 'btn btn-lg btn-success col-12 col-md-4 offset-md-4'])?>
        </div>
    </div>
</div>

<?php
$js = <<<JS
$(function(){
    $('.wrap').addClass('h-100');
    $('#start').fadeIn();
});
JS;
$this->registerJs($js);
?>