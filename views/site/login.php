<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$login_title = 'Вход';
$reg_title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class="col-12 col-md-6">
            <h1 class="text-primary"><?=Html::encode($login_title) ?></h1>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"pl-0 col-12 col-lg-10\">{input}</div>\n<div class=\"pl-0 text-danger col-12\">{error}</div>",
                    'labelOptions' => ['class' => 'control-label'],
                ],
            ]); ?>

            <?= $form->field($login_model, 'username')->label('Логин') ?>

            <?= $form->field($login_model, 'password')->passwordInput()->label('Пароль') ?>

            <?= $form->field($login_model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col pl-0\">{input} {label}</div>\n<div class=\"col pl-0\">{error}</div>",
            ])->label('Запомнить меня') ?>

            <div class="form-group">
                <div class="col-12 pl-0">
                    <?= Html::submitButton('Войти', ['class' => 'col-12 col-md-10 btn btn-success', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-12 col-md-6 border-left">
                <h1 class="text-primary"><?= Html::encode($reg_title) ?></h1>
                <h5 class="text-success"><?=Html::encode($reg_msg)?></h5>
            <?php $form = ActiveForm::begin([
                'id' => 'registration-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"pl-0 col-12 col-lg-10\">{input}</div>\n<div class=\"pl-0 col-12 text-danger\">{error}</div>",
                    'labelOptions' => ['class' => 'control-label'],
                ],
            ]); ?>

            <?= $form->field($reg_model, 'username')->label('Логин') ?>

            <?= $form->field($reg_model, 'password')->passwordInput()->label('Пароль') ?>
            <?= $form->field($reg_model, 'fio')->textInput()->label('ФИО')?>

            <div class="form-group">
                <div class="col-12 pl-0">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'col-12 col-md-10 btn btn-success', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
