<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap bg-dark text-light">
<!--    --><?php
//    NavBar::begin([
//        'brandLabel' => 'PDF Mail',
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar navbar-expand-lg navbar-light bg-light',
//        ],
//    ]);
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => [
//            !Yii::$app->user->isGuest ? ['label' => 'Создать письмо', 'url' => ['/letters/create']] : '',
//            !Yii::$app->user->isGuest ? ['label' => 'Просмотреть письма', 'url' => ['/letters/cabinet']] : '',
//            Yii::$app->user->isGuest ? (['label' => 'Зарегистрироваться', 'url' => ['/site/registration']]) : '',
//            Yii::$app->user->isGuest ? (['label' => 'Войти', 'url' => ['/site/login']]):(
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Выйти (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            ),
//        ],
//    ]);
//    NavBar::end();
//    ?>

    <div class="container h-100 pt-3">
<!--        --><?//= Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<?=Html::a("^", '#', ['id' => 'on_top', 'class' => 'shadow btn btn-primary py-1'])?>
<footer class="footer bg-dark text-light border-0">
    <div class="container">
        <p class="text-center">Copyright &copy; PDF Mail <?= date('Y') ?></p>
    </div>
</footer>

<?php
if (Yii::$app->controller->id.'/'.Yii::$app->controller->action->id == 'letters/create'):
    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/css/suggestions.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/js/jquery.suggestions.min.js"></script>

    <script>
        $("#letterform-sender_adress, #letterform-recipient_adress").suggestions({
            token: "8902f1b2988e0ac262005d1411c7d76cfdf8b905",
            type: "ADDRESS",
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
    </script>
    <script>
        //    $("#address").suggestions({
        //        token: "8902f1b2988e0ac262005d1411c7d76cfdf8b905",
        //        type: "ADDRESS",
        //        /* Вызывается, когда пользователь выбирает одну из подсказок */
        //        onSelect: function(suggestion) {
        //            console.log(suggestion);
        //        }
        //    });

        function join(arr /*, separator */) {
            var separator = arguments.length > 1 ? arguments[1] : ", ";
            return arr.filter(function(n){return n}).join(separator);
        }

        function formatCity(suggestion) {
            var address = suggestion.data;
            if (address.city_with_type === address.region_with_type) {
                return address.settlement_with_type || "";
            } else {
                return join([
                    address.city_with_type,
                    address.settlement_with_type]);
            }
        }

        var
            token = "8902f1b2988e0ac262005d1411c7d76cfdf8b905 ",
            type  = "ADDRESS",
            recipient_region = $("#recipient_region"),
            recipient_city   = $("#recipient_city"),
            recipient_street = $("#recipient_street"),
            recipient_house  = $("#recipient_house"),

            sender_region = $("#sender_region"),
            sender_city   = $("#sender_city"),
            sender_street = $("#sender_street"),
            sender_house  = $("#sender_house");

        // регион и район
        recipient_region.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "region-area"
        });

        sender_region.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "region-area"
        });

        // город и населенный пункт
        recipient_city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement",
            constraints: recipient_region,
            formatSelected: formatCity
        });

        sender_city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement",
            constraints: sender_region,
            formatSelected: formatCity
        });

        // улица
        recipient_street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: recipient_city,
            count: 15
        });
        sender_street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: sender_city,
            count: 15
        });

        // дом
        recipient_house.suggestions({
            token: token,
            type: type,
            hint: false,
            noSuggestionsHint: false,
            bounds: "house",
            constraints: recipient_street,
            onSelect: function(suggestion) {
                $('#letterform-recipient_adress').val(JSON.stringify(suggestion.data));
            }
        });

        sender_house.suggestions({
            token: token,
            type: type,
            hint: false,
            noSuggestionsHint: false,
            bounds: "house",
            constraints: sender_street,
            onSelect: function(suggestion) {
                $('#letterform-sender_adress').val(JSON.stringify(suggestion.data));
            }
        });

        $('#recipient_flat').on('change',function(){
            $('#letterform-recipient_flat').val($('#recipient_flat').val());
        });

        $('#sender_flat').on('change',function(){
            $('#letterform-sender_flat').val($('#sender_flat').val());
        });




        //    console.log(recipient_region.suggestions());
        //    console.log(recipient_city.suggestions());
        //    console.log(recipient_street.suggestions());
        console.log(recipient_house.suggestions());

        //    console.log(sender_region.suggestions());
        //    console.log(sender_city.suggestions());
        //    console.log(sender_street.suggestions());
        console.log(sender_house.suggestions());
    </script>
<?php endif?>

<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 0) {
                $('#on_top').fadeIn();
            } else {
                $('#on_top').fadeOut();
            }
        });
        $('#on_top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
