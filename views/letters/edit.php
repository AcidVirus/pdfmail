<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use \app\models\LetterTemplate;
use yii\helpers\ArrayHelper;

$this->title = 'Новый конверт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1 class="text-primary"><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'letter-form',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n<div class=\"pl-0 col-12 text-danger\">{error}</div>",
            'labelOptions' => ['class' => 'control-label'],
        ],
    ]); ?>
    <?php
    $templates = LetterTemplate::find()->all();
    $items = ArrayHelper::map($templates, 'id', 'link');
    ?>

    <div class="row">
        <div class="col-12">
        <?= $form->field($model, 'template_id')->dropDownList($items,['id' => 'temp_id', 'class' => 'd-none'])->label('')?>

        <div class="row">
            <div class="col-md-4">
                <h4>Получатель письма <p class="small">(в именительном падеже)</p></h4>
                <?= $form->field($model, 'recipient_fam')->label('Фамилия') ?>
                <?= $form->field($model, 'recipient_name')->label('Имя')?>
                <?= $form->field($model, 'recipient_patr')->label('Отчество')?>

                <?= $form->field($model, 'recipient_adress')->label('Адрес получателя')?>
            </div>
            <div class="col-md-4">
                <h4>Отправитель письма<p class="small">(в именительном падеже)</p></h4>
                <?= $form->field($model, 'sender_fam')->label('Фамилия')?>
                <?= $form->field($model, 'sender_name')->label('Имя')?>
                <?= $form->field($model, 'sender_patr')->label('Отчество')?>
                <?= $form->field($model, 'sender_adress')->label('Адрес отправителя')?>
            </div>
            <div class="col-md-4">
                <h4>Выберите конверт</h4>
                <?=Html::tag('a', 'Предыдущий',['id' => 'prev_letter', 'class' => 'btn btn-primary']);?>
                <?=Html::tag('a', 'Следующий',['id' => 'next_letter', 'class' => 'float-right btn btn-primary']);?>
                <?=Html::img('',['id' => 'letter_template', 'class' => 'col-12'])?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::a('Назад', Url::toRoute(['letters/cabinet']),['class' => 'btn btn-primary btn-lg']) ?>
            <?= Html::submitButton('Создать конверт', ['class' => 'btn btn-success btn-lg', 'name' => 'letter-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$url = Url::toRoute(["letters/get-thumbnail"]);
$size = 400;
$js=<<<JS
    $(function(){
        get_letter_template(1,$size);
        
        $("#next_letter").on("click", function(e) {
            get_next_template($size);      
        });
    
        $("#prev_letter").on("click", function(e) {
            get_prev_template($size);      
        });
    });

    function get_next_template(size){
        id = $('#temp_id').val();
        if(id == $('#temp_id option').length){
            return;
        }
        get_letter_template(++id,size);
        $('#temp_id option[value=' + id + ']').prop('selected', true);
    }
    
    function get_prev_template(size){
        id = $('#temp_id').val();
        if(id == 1){
            return;
        }
        get_letter_template(--id,size);
        $('#temp_id option[value=' + id + ']').prop('selected', true);
    }

    function get_letter_template(id, size){
        
        $.ajax({
                url: "$url",
                type : "POST",
                data : {template_id:id,size:size},
                success : function(res){
                   $('#letter_template').attr('src', res);
                },
            error : function(){
                alert("Ошибка при отправке данных: перезагрузите страницу!");
            }
            })
            return false;  
    }
JS;
$this->registerJs($js);
?>
