<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-contact">
    <h1 class="text-primary">Ваше письмо успешно создано!</h1>
        <div class="row">
            <div class="col">
                <?=Html::a('Скачать письмо',Url::toRoute(['/letters/download','file' => $model->pdf_link]),['class' => 'btn btn-success']);?>
            </div>
            <div class="col">
                <?=Html::a('Открыть в браузере', $model->pdf_link,['class' => 'btn btn-primary']);?>
            </div>
            <div class="col">
                <?=Html::a('Личный кабинет', Url::toRoute('/letters/cabinet'),['class' => 'btn btn-info']);?>
            </div>
        </div>
</div>
