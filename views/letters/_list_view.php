<?php

use yii\helpers\Html;
use yii\helpers\Url;

$recipient_adress = json_decode($model->sender_adress);
$sender_adress = json_decode($model->recipient_adress);

$sender_adress = $sender_adress->region_with_type . ", " . $sender_adress->city_with_type . ", " . $sender_adress->street_with_type . ", " . $sender_adress->house_type . '. ' . $sender_adress->house;
$recipient_adress = $recipient_adress->region_with_type . ", " . $recipient_adress->city_with_type . ", " . $recipient_adress->street_with_type . ", " . $recipient_adress->house_type . '. ' . $recipient_adress->house;
?></pre>
<div class="col-12 py-3 secondary my-3 letter-info rounded">
    <div class="row">
        <div class="col-6 text-success pr-0 text-right">ФИО получателя:</div><div class="col-6"><?=$model->recipient_fio;?></div>
    </div>
    <div class="row">
        <div class="col-6 text-success pr-0 text-right">Адрес получателя:</div><div class="col-6"><?=$recipient_adress;?></div>
    </div>
    <div class="row">
        <div class="col-6 text-success pr-0 text-right">ФИО отправителя:</div><div class="col-6"><?=$model->sender_fio;?></div>
    </div>
    <div class="row">
        <div class="col-6 text-success pr-0 text-right">Адрес отправителя:</div><div class="col-6"><?=$sender_adress;?></div>
    </div>
    <div class="row">
        <div class="col-6 text-success pr-0 text-right">Дата создания:</div><div class="col-6"><?=$model->creation_date;?></div>
    </div>

    <div class="row mt-3 justify-content-center">
        <div class="col-12 col-lg-8 offset-2">
            <?=Html::a("Скачать", Url::toRoute(['letters/download', 'file' => $model->pdf_link]), ['class' => 'col-5 col-lg-2 btn btn-success'])?>
            <?=Html::a("Открыть", Url::toRoute(['letters/view', 'file' => $model->pdf_link]), ['class' => 'col-5 col-lg-2 btn btn-primary'])?>
            <?=Html::a("Редактировать", Url::toRoute(['letters/create', 'id' => $model->id]), ['class' => 'col-lg-3 col-5 mt-2 mt-lg-0 btn btn-secondary'])?>
            <?=Html::a("Удалить", Url::toRoute(['letters/delete', 'id' => $model->id]), ['class' => 'delete col-5 col-lg-2 mt-2 mt-lg-0 btn btn-danger'])?>
        </div>
    </div>
</div>

<?php
$js = <<<JS
    $('.delete').on('click', function(){
      if(!confirm('Вы уверены, что хотите удалить конверт?')){
          return false;
      }  
    })
JS;
$this->registerJs($js);
?>