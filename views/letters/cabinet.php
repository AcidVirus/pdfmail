<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\imagine\Image;
use \app\models\Letter;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1 class="text-primary"><?= Html::encode($this->title) ?></h1>
    <h4>Добро пожаловать, <?= Html::encode(Yii::$app->user->identity->fio) ?>!</h4>
    <div class="row">
        <div class="col-6 col-lg-3">
            <?=Html::a('Создать новый конверт', Url::toRoute('letters/create'), ['class' => 'col-12 btn btn-success'])?>
        </div>
        <div class="col-6 col-lg-3 offset-lg-6">
            <?=
            Html::beginForm(['/site/logout'], 'post')
            .Html::submitButton('Выйти',['class' => 'col-12 btn btn-success'])
            .Html::endForm();
            ?>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <?php
        $dataProvider = new ActiveDataProvider([
            'query' => Letter::find()->where(['user_id' => Yii::$app->user->id]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

//        echo GridView::widget([
//            'dataProvider' => $dataProvider,
//            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
//                'recipient_fio:ntext:ФИО получателя',
//                'recipient_adress:ntext:Адрес получателя',
//                'sender_fio:ntext:ФИО отправителя',
//                'sender_adress:ntext:Адрес отправителя',
//                'creation_date:date:Дата создания',
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'header'=>'Письма',
//                    'template' => '{download-by-id}',
//                    'buttons' => [
//                        'download-by-id' => function($data){
//                            return Html::a('Скачать', $data,['class' => 'btn btn-primary']);
//                        },
//                    ],
//                ],
//                ['class' => 'yii\grid\ActionColumn',
//                    'header'=>'Действия',
//                    'template' => '{view}', //'{view} {edit} {delete}',
//                    'buttons' => [
//                        'view' => function($data){
//                            return Html::a('Просмотр',$data,['class' => 'btn btn-primary']);
//                        },
//                    ],
//                ],
//            ],
//
//            'options' => ['class' => 'table-responsive'],
//            'tableOptions' => ['class' => 'table table-condensed'],
//        ]);
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'class' => 'letters-view',
                'itemView' => '_list_view',
                'pager' => [
                    'firstPageLabel' => 'В начало',
                    'lastPageLabel' => 'В конец',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<',
                    'maxButtonCount' => 3,
                ],
            ]);
        ?>
        </div>
    </div>
</div>