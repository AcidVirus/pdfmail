<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use \app\models\LetterTemplate;
use yii\helpers\ArrayHelper;

$this->title = 'Новый конверт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letters-create">
    <h1 class="text-primary"><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'letter-form',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n<div class=\"pl-0 col-12 text-danger\">{error}</div>",
            'labelOptions' => ['class' => 'control-label'],
        ],
    ]); ?>
    <?php
    $templates = LetterTemplate::find()->all();
    $items = ArrayHelper::map($templates, 'id', 'link');
    ?>

    <div class="row">
        <div class="col-12">
        <span class="d-none v-0">
            <?= $form->field($model, 'template_id')->dropDownList($items,['id' => 'temp_id', 'class' => 'd-none'])->label('')?>
            <?= $form->field($model, 'recipient_adress')->textInput(['class' => 'd-none'])->label('')?>
            <?= $form->field($model, 'sender_adress')->textInput(['class' => 'd-none'])->label('')?>
            <?= $form->field($model, 'recipient_flat')->textInput(['class' => 'd-none'])->label('')?>
            <?= $form->field($model, 'sender_flat')->textInput(['class' => 'd-none'])->label('')?>
        </span>
        <div class="row">
            <div class="col-md-4">
                <h4>Получатель письма <p class="small">(в именительном падеже)</p></h4>
                <?= $form->field($model, 'recipient_fam')->textInput(['placeholder' => 'Фамилия'])->label('Фамилия') ?>
                <?= $form->field($model, 'recipient_name')->textInput(['placeholder' => 'Имя'])->label('Имя')?>
                <?= $form->field($model, 'recipient_patr')->textInput(['placeholder' => 'Отчество'])->label('Отчество')?>


                <label>Адрес получателя</label>
                <div class="row mx-1">
                    <?=Html::textInput("","",['id' => 'recipient_region', 'required' => true, 'placeholder' => 'Область', 'class' => 'mb-2 form-control'])?>
                    <?=Html::textInput("","",['id' => 'recipient_city', 'required' => true, 'placeholder' => 'Город', 'class' => 'mb-2 form-control'])?>
                    <?=Html::textInput("","",['id' => 'recipient_street', 'required' => true, 'placeholder' => 'Улица', 'class' => 'mb-2 form-control'])?>
                    <?=Html::textInput("","",['id' => 'recipient_house', 'required' => true, 'placeholder' => 'Дом', 'class' => 'mb-2 col-5 form-control'])?>
                    <?=Html::textInput("","",['id' => 'recipient_flat', 'placeholder' => 'Квартира', 'class' => 'col-5 ml-auto mr-0 form-control'])?>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Отправитель письма<p class="small">(в именительном падеже)</p></h4>
                <?= $form->field($model, 'sender_fam')->textInput(['placeholder' => 'Фамилия'])->label('Фамилия')?>
                <?= $form->field($model, 'sender_name')->textInput(['placeholder' => 'Имя'])->label('Имя')?>
                <?= $form->field($model, 'sender_patr')->textInput(['placeholder' => 'Отчество'])->label('Отчество')?>

                <label>Адрес отправителя</label>
                <div class="row mx-1">
                    <?=Html::textInput("","",['id' => 'sender_region', 'required' => true, 'placeholder' => 'Область', 'class' => 'mb-2 form-control'])?>
                    <?=Html::textInput("","",['id' => 'sender_city', 'required' => true, 'placeholder' => 'Город', 'class' => 'mb-2 form-control'])?>
                    <?=Html::textInput("","",['id' => 'sender_street', 'required' => true, 'placeholder' => 'Улица', 'class' => 'mb-2 form-control'])?>
                    <?=Html::textInput("","",['id' => 'sender_house', 'required' => true, 'placeholder' => 'Дом', 'class' => 'mb-2 col-5 form-control'])?>
                    <?=Html::textInput("","",['id' => 'sender_flat', 'placeholder' => 'Квартира', 'class' => 'col-5 ml-auto mr-0 form-control'])?>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Выберите конверт</h4>
                <div class="letter-info">
                    <?=Html::tag('a', 'Предыдущий',['id' => 'prev_letter', 'class' => 'col-4 col-sm-6 col-md-4 btn btn-secondary']);?>
                    <?=Html::tag('a', 'Следующий',['id' => 'next_letter', 'class' => 'col-4 col-sm-6 col-md-4 float-right btn btn-secondary']);?>
                    <?=Html::img('',['id' => 'letter_template', 'class' => 'col-12'])?>
                </div>
            </div>
        </div>
        <div class="form-group mt-3">
            <?= Html::a('Назад', Url::toRoute(['letters/cabinet']),['class' => 'btn btn-primary btn-lg']) ?>
            <?= Html::submitButton('Создать конверт', ['class' => 'float-right btn btn-success btn-lg', 'name' => 'letter-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$url = Url::toRoute(["letters/get-thumbnail"]);
$size = 400;
$js=<<<JS
    $(function(){
        get_letter_template(1,$size);
        
        $("#next_letter").on("click", function(e) {
            get_next_template($size);      
        });
    
        $("#prev_letter").on("click", function(e) {
            get_prev_template($size);      
        });
        
        $('#letter-form').submit(function(e){
            if (!$('#letterform-sender_adress').val && !$('#letterform-sender_adress').val){
                alert('Вы не указали адрес!');
                e.preventDefault();
            };
        });
    });

    function get_next_template(size){
        id = $('#temp_id').val();
        if(id == $('#temp_id option').length){
            return;
        }
        get_letter_template(++id,size);
        $('#temp_id option[value=' + id + ']').prop('selected', true);
    }
    
    function get_prev_template(size){
        id = $('#temp_id').val();
        if(id == 1){
            return;
        }
        get_letter_template(--id,size);
        $('#temp_id option[value=' + id + ']').prop('selected', true);
    }

    function get_letter_template(id, size){
        
        $.ajax({
                url: "$url",
                type : "POST",
                data : {template_id:id,size:size},
                success : function(res){
                   $('#letter_template').attr('src', res);
                },
            error : function(){
                alert("Ошибка при отправке данных: перезагрузите страницу!");
            }
            })
            return false;  
    }    
JS;
$this->registerJs($js);
?>
