<?php

namespace app\models;

class Letter extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%letter}}';
    }

    public static function selectAllUserLetters($user_id){
        return self::findAll(['user_id' => $user_id]);
    }
}