<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\petrovich\Petrovich;
use kartik\mpdf\Pdf;
use yii\imagine\Image;
use yii\helpers\Html;

class LetterForm extends Model
{
    public $recipient_fam;
    public $recipient_name;
    public $recipient_patr;
    public $recipient_adress;
    public $recipient_flat;
    public $sender_fam;
    public $sender_name;
    public $sender_patr;
    public $sender_adress;
    public $sender_flat;
    public $template_id;
    public $pdf_link;

    public function rules()
    {
        return [
            [['recipient_fam', 'recipient_name', 'recipient_patr', 'recipient_adress', 'sender_fam', 'sender_name', 'sender_patr', 'sender_adress', 'template_id'], 'required', 'message' => "Поле обязательно для заполнения!"],
            ['template_id', 'templateValidation'],
        ];
    }

    public function templateValidation($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->templateExist($this->template_id)) {
                $this->addError($attribute, 'Указанного шаблона письма не существует!');
            }
        }
    }

    public function addLetter()
    {
        $recipient_fio = $this->fioConvert($this->recipient_fam, $this->recipient_name, $this->recipient_patr);
        $sender_fio = $this->fioConvert($this->sender_fam, $this->sender_name, $this->sender_patr, Petrovich::CASE_GENITIVE);
        $recipient_adress = json_decode($this->recipient_adress);
        $sender_adress = json_decode($this->sender_adress);

        $letter_data = [
            "recipient_fio" => $recipient_fio,
            "recipient_city" => $recipient_adress->region_with_type . ", " . $recipient_adress->city_with_type,
            "recipient_adress" => $recipient_adress->street_with_type . ", " . $recipient_adress->house_type . '. ' . $recipient_adress->house .( $this->recipient_flat ? ' - ' . $this->recipient_flat : ''),
            "recipient_postal_code" => $recipient_adress->postal_code,

            "sender_fio" => $sender_fio,
            "sender_city" => $sender_adress->region_with_type . ", " . $sender_adress->city_with_type,
            "sender_adress" => $sender_adress->street_with_type . ", " . $sender_adress->house_type . '. ' . $sender_adress->house .( $this->sender_flat ? ' - ' . $this->sender_flat : ''),
            "sender_postal_code" => $sender_adress->postal_code,
        ];

        $this->pdf_link = $this->createPdfLetter($this->template_id, $letter_data);

        $letter = new Letter();
        $letter->recipient_fio = $recipient_fio;
        $letter->recipient_adress = $this->recipient_adress;

        $letter->sender_fio = $sender_fio;
        $letter->sender_adress = $this->sender_adress;

        $letter->template_id = $this->template_id;
        $letter->pdf_link = $this->pdf_link;
        $letter->user_id = Yii::$app->user->id;
        $letter->save();

        return true;
    }

    public function fioConvert($fam, $name, $patr, $case = Petrovich::CASE_DATIVE){
        $petrovich = new Petrovich();
        $petrovich->setGender($petrovich->detectGender($patr));

        $fio = "";
        $fio = $petrovich->lastname($fam, $case).' ';
        $fio .= $petrovich->firstname($name, $case).' ';
        $fio .= $petrovich->middlename($patr, $case);



        return $fio;
    }

    private function templateExist($id)
    {
        return LetterTemplate::findOne(['id' => $id]) ? true : false;
    }


    private function createPdfLetter($template_id, $letter_data)
    {
        $letter_path = $this->generateLetter($template_id, $letter_data);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        $pdf = new Pdf();
        $pdf = $pdf->api;
        $pdf->WriteHtml(Html::img($letter_path));
        unlink($letter_path);

        $pdf_dir_path = 'pdf_letters/';
        $pdf_name = $this->getRandomFileName(Yii::getAlias('@webroot/'.$pdf_dir_path),'pdf');
        $pdf->Output(Yii::getAlias('@webroot/'.$pdf_dir_path.$pdf_name), 'F');
        return $pdf_dir_path.$pdf_name;
    }

    private function generateLetter($template_id, $letter_data)
    {
        $temp_dir_path = Yii::getAlias('@webroot/outimages/');
        $img_path = $temp_dir_path . $this->getRandomFileName($temp_dir_path,'jpg');

        $positions = json_decode(LetterTemplate::selectTemplateJsonById($template_id), true);
        $image = Yii::getAlias('@webroot/'.LetterTemplate::selectTemplateLinkById($template_id));
        $fontFile = Yii::getAlias('@webroot/fonts/roboto/Roboto.woff');
        $fontOptions = [
            'size' => 30,    // Размер шрифта
            'color' => '000', // цвет шрифта
        ];
        $offset_x = 73;

        foreach ($positions as $position => $value) {
            if ($position == 'recipient_postal_code'){
                for ($i=0;$i<6;$i++){
                    $image = Image::text(
                        $image,                     // Картинка, на которой рисуем текст
                        $letter_data[$position][$i],    // Текст
                        $fontFile,                  // Путь к файлу шрифта
                        $value,                     // Отступ от левого края картинки
                        $fontOptions
                    )->save($img_path, ['quality' => 100]);
                    $value[0] += $offset_x;
                }
            }else{
                $image = Image::text(
                    $image,                     // Картинка, на которой рисуем текст
                    $letter_data[$position],    // Текст
                    $fontFile,                  // Путь к файлу шрифта
                    $value,                     // Отступ от левого края картинки
                    $fontOptions
                )->save($img_path, ['quality' => 100]);
            }
        }

        return $img_path;
    }

    private function getRandomFileName($path, $extension='')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';

        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));

        return $name . $extension;
    }
}


