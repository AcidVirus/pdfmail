<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $username;
    public $password;
    public $fio;

    public function rules()
    {
        return [
            [['username', 'password', 'fio'], 'required', 'message' => "Поле обязательно для заполнения!"],
            ['username', 'unique', 'targetClass' => '\app\models\User'],
            ['password', 'match', 'pattern' => '/^([a-zA-Z0-9-_]){6,50}$/'],
        ];
    }

    public function register()
    {
        if ($this->validate()) {
            $model = new User();
            $model->username = $this->username;
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $model->fio = $this->fio;
            $model->accessToken = $this->username."Token";
            $model->authKey = $this->username."Key";
            $model->save();
            return true;
        }
        return false;
    }
}
