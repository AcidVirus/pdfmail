<?php

namespace app\models;

class LetterTemplate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%letter_template}}';
    }

    public static function selectTemplateJsonById($template_id){
        return self::find()->select('template_json')->where(['id' => $template_id])->one()['template_json'];
    }

    public static function selectTemplateLinkById($template_id){
        return self::find()->select('link')->where(['id' => $template_id])->one()['link'];
    }
}